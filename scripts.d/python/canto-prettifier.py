#!/usr/bin/env python3

import re
import os
import html

txtDir    = "../../of-txt.d/"
cantoDir  = txtDir + "cantos/"

def htmlToUnicode(old, new):
    new.write(html.unescape(old.read()))

def prettify():
    for cantoFile in os.listdir(cantoDir):
        print("Prettifying: " + cantoFile)
        if re.search("\.txt$", cantoFile): # check: only operate on txt files
            # File setup
            oldName = cantoDir + cantoFile
            newName = cantoDir + cantoFile + ".new"
            old = open(oldName, "r")
            new = open(newName, "w")
            # Prettify
            htmlToUnicode(old, new)
            # File closing
            old.close()
            new.close()
            os.remove(oldName)
            os.rename(newName, oldName)

prettify()
