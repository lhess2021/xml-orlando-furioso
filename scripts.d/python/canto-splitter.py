#!/usr/bin/env python3

"""
Split the massive OF file into separate canto text files
named as "canto-<num>.txt".
"""

import time
import os
import re  # find and replace
import shutil  # move tmp file for overwrite

# Globals
txtDir    = "../../of-txt.d/"
OFtxtFile = txtDir + "original/orlando-furioso-gutenberg.txt"
cantoDir  = txtDir + "cantos/"
numCantos = 46

def cantoFileName(num):
    cantoFile = cantoDir + "canto-" + str(num) + ".txt"
    return cantoFile

def splitCantos():
    """
    Split the original text into canto files between
    the CANTO PRIMO... CANTO SECONDO... CANTO TERZO delimiters.
    """
    i = 1
    copyToggle = False;
    # Split with regex
    with open(OFtxtFile, "r") as OF:
        # re.S is the DOTALL flag, '.' matches \n
        # Use a lookahead assertion '(?=...)' to not consume the 'CANTO ...' of the next canto
        # Test for 'FINIS' to get the last canto as well
        for canto in re.findall('(CANTO .*?\n)(.*?)(?=CANTO|FINIS)', OF.read(), re.S):
            cantoFile = cantoFileName(i)
            with open(cantoFile, "w") as cf:
                cf.write(canto[1]) # this group matches the canto body
            i += 1

splitCantos()
