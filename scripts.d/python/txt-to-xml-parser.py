#!/usr/bin/env python3

"""
Parse each canto text file to XML
"""

import time
import os
import re  # find and replace
import shutil  # move tmp file for overwrite
import html

# GLOBALS
txtDir    = "../../of-txt.d/"
xmlDir    = "../../of-xml.d/"
OFtxtFile = txtDir + "original/orlando-furioso-gutenberg.txt"
cantoDir  = txtDir + "cantos/"
numCantos = 46

def getXMLFileName(cantoFile):
    #print("cf: " + cantoFile)
    xmlFile = re.sub("txt", "xml", cantoFile) # change extension 'txt' -> 'xml'
    #print("xf: " + xmlFile)
    return xmlFile

def prepareForTraverse(cantoText, find, replace, negate):
    """
    Substitute find with replace.
    If negate is True, substitute replace with find.
    """
    if negate == False:
        replacedText = cantoText.replace(find, replace)
    else:
        replacedText = cantoText.replace(replace, find)
    return replacedText

def addWordTags(textFile):
    """
    - [x] Open file as one huge string (keep track of newlines)
    - [x] Replace every space with a dummy char not used in the text, like `~`
    - [x] Traverse the big file-string from beginning to end
        - [x] Define a "word-chars" group: letters, apostrofes
        - [x] If you encounter a char that's a word-char
              put a `<w>` in front of it and move to the next.
            - [x] If the next char is also a word-char, move on
                  to the next because we're still in a word.
            - [x] If the next char is not a word-char,
                  put a `</w>` in front of it and move to the next.
    - [x] Replace all `~` with spaces and get back the newlines
    - [x] Write to XML file
    """
    xmlCanto = getXMLFileName(textFile)
    #print(xmlCanto)
    with open(xmlCanto, "w") as xml:
        with open(textFile, "r") as txt:
            originalText = txt.read()
            # Prepare canto for traversal
            preparedText = prepareForTraverse(originalText, "\n", "+", False)
            preparedText = prepareForTraverse(preparedText, " ", "~", False)

            # Variables
            wordChars = "[a-zA-ZÀ-ÿ0-9‘’']"
            openWord = "<w>"
            closeWord = "</w>"
            insideWord = False
            emDash = "—"

            # Traverse canto
            for char in preparedText:
                if char == emDash:
                    xml.write("<dialogue>—</dialogue>")
                elif re.match(wordChars, char):
                    if insideWord == False:
                        xml.write(openWord)
                        xml.write(char)
                        insideWord = True
                    else:
                        xml.write(char)
                else:
                    if char == "~": # placeholder for spaces
                        if insideWord == True:
                            xml.write(closeWord + " ")
                            insideWord = False
                        else:
                            xml.write(" ")
                    elif char == "+": # placeholder for newlines
                        if insideWord == True:
                            xml.write(closeWord + "\n")  # line ends with word
                            insideWord = False
                        else:
                            xml.write("\n")  # line ends with punctuation
                    elif insideWord == True:
                        xml.write(closeWord + char)  # punctuation right after the word
                        insideWord = False
                    else:
                        xml.write(char)  # punctuation after punctuation
        txt.close()
    xml.close()

# def addOctaveFormatting(xmlCantoFile):
#     """
#     Replace \n<w>octave_num</w>\n with <octave o_num="num">
#     """
#     xmlCanto = getXMLFileName(xmlCantoFile)
#     tmpFile = "." + xmlCantoFile + ".tmp"
#     with open(xmlCanto, "r") as xml:
#         with open(tmpFile, "w") as tmp:

#             tmp.close()
#         xml.close()

def addLineTags(xmlCantoFile):
    """
    Add <l l_num="n"> to the beginning of each line number n,
    where n is the number in the octave (take mod 8).

    Add </l> to the end of each line.
    """
    tmpFile = xmlCantoFile + ".tmp"
    with open(xmlCantoFile, "r") as xml:
        with open(tmpFile, "w") as tmp:
            l = 1
            for line in xml:
                # replace '\n' in line with ' </l>\n'
                newline = re.sub("\n", " </l>\n", line)
                l = l % 8 if l % 8 != 0 else 8 # catch 8 % 8 = 0, make it 8 instead
                newline = '<l l_num="' + str(l) + '"> ' + newline # take mod 8 to make octave line nums
                tmp.write(newline)
                l += 1
    closeCopyRemoveTmp(tmpFile, tmp, xmlCantoFile, xml)

def getLineNum(line):
    """
    Extracts the line number attribute from a canto xml line.
    """
    lineNum = int(line.split('"')[1])
    return lineNum

def addOctaveTags(xmlCantoFile):
    """
    Add <octave o_num="n"> and </octave> tags around every 8 lines.
    """
    tmpFile = xmlCantoFile + ".tmp"
    with open(xmlCantoFile, "r") as xml:
        lastLine = xml.readlines()[-1]  # still a \n at the end
        lastLineNum = getLineNum(lastLine)
        xml.seek(0)
        with open(tmpFile, "w") as tmp:
            # Write the first opening octave tag
            newline = '<octave o_num="1">\n' + xml.readline()
            tmp.write(newline)
            o = 2 # we put the first octave tag manually, so start at tag 2
            for line in xml:
                lineNum = getLineNum(line)
                if lineNum == 8:
                    octaveNum = str(o)
                    newLineSub = '\n</octave>\n<octave o_num="' + octaveNum + '">\n'
                    newLine = re.sub("\n", newLineSub, line)
                    tmp.write(newLine)
                    o += 1
                else:
                    tmp.write(line)
            tmp.close()
        # Delete the current last line
        with open(tmpFile, "r") as tmp:
            lines = tmp.readlines()
            lines = lines[:-1]
            tmp.close()
        with open(tmpFile, "w") as tmp:
            for line in lines:
                tmp.write(line)
    closeCopyRemoveTmp(tmpFile, tmp, xmlCantoFile, xml)

def wrapWithTags(tag, xmlCantoFile):
    """
    Add tags e.g. <canto>...</canto> around the whole xml.
    """
    tmpFile = xmlCantoFile + ".tmp"
    with open(xmlCantoFile, "r") as xml:
        with open(tmpFile, "w") as tmp:
            cantoNum = re.findall(r'\d+', xmlCantoFile)[0]
            tmp.write('<canto c_num="' + cantoNum + '">\n')
            for line in xml.readlines():
                tmp.write(line)
            tmp.write('</canto>')
    closeCopyRemoveTmp(tmpFile, tmp, xmlCantoFile, xml)


def closeCopyRemoveTmp(tmpFile, tmp, xmlCantoFile, xml):
    tmp.close()
    xml.close()
    shutil.copy(tmpFile, xmlCantoFile)
    os.remove(tmpFile)

def main():
    for textFile in os.listdir(cantoDir):
        textFile = cantoDir + textFile
        xmlCantoName = getXMLFileName(textFile)
        #print("xmlCantoName: " + xmlCantoName)
        #print("textFile: " + textFile)
        if re.search("\.txt$", textFile): # check: only operate on txt files
            addWordTags(textFile)
            addLineTags(xmlCantoName)
            addOctaveTags(xmlCantoName)
            wrapWithTags("canto", xmlCantoName)

main()
