#!/usr/bin/env bash

# Print octaves to the terminal from a list of octave numbers.
# The canto to use is the first argument to the script.

# Pipe the output into less for easy reading.

declare -a cantos=(1 2 3 10) # and so on

for n in "${cantos[@]}";
do
    echo -e "\nCanto $n"
    xp="//octave[@o_num=\"$n\"]"
    xmlstarlet sel -t -v "$xp" "$1"
done
