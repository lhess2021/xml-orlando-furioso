#!/usr/bin/env sh

# A handler script to run an xpath script on each canto file.

# The script should be in the cwd, and should be written as './<script-name>'
script="$1"

xmlDir='../../of-xml.d/cantos'

for canto in "$xmlDir"/*;
do
    $script $canto
done
