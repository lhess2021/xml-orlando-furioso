#!/usr/bin/env sh

# Calculates the number of words in a canto that contain
# any of the following sequences of letters: 'or', 'OR', 'oR', 'Or'.
# Requires the command-line program 'xmlstarlet'
#  - Debian: apt install xmlstarlet
#  - Mac:    brew install xmlstarlet

# The argument of this script is the canto that is searched.
# To calculate the number of words, pipe the output into some
# program like wc (word count) or awk.

# The xpath command to run. The translate function is used to get a
# case-insensitive search. The '//w[contains(...)]' searches all 'w' (word)
# nodes and returns those which contain the specified sequence, e.g. 'or'.
xCommand="//w[contains(\
    translate(., 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'), 'or')]"

# xmlstarlet
#  -> sel: select data or query XML document
#      -> -t: template
#          -> -v: print value of xpath query
xmlstarlet sel -t -v "$xCommand" "$1"
